import * as React from 'react';
import {Text, Button, TextInput, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Unsplash, {toJson} from 'unsplash-js';
import styled from 'styled-components/native';

class Home extends React.Component {
  private unsplash: Unsplash;
  constructor(props: any) {
    super(props);

    this.state = {
      users: {
        results: [],
        total: 0,
        total_pages: 0,
      },
    };

    this.unsplash = new Unsplash({
      accessKey:
        'aa2f3c3be8125f1fc86e3007153420c4e446c19b7b0c6d80a6257b281c9a0dc5',
      secret:
        'a5ab4ed2efdc772dca8d5636a26c0d897907df38cd92baa9067e57093d9596b5',
    });
  }

  async onChangeText(text: string) {
    try {
      console.log('your text =>', text);
      this.unsplash.search
        .users(text, 1)
        .then(toJson)
        .then((json) =>
          this.setState({users: json}, () => console.log(this.state.users)),
        );
    } catch (e) {
      console.log('there was an error =>', e);
    }
  }

  renderUsers(navigation: any) {
    if (this.state.users.total === 0) {return;}
    return this.state.users.results.map((result: any, index: number) => {
      return [
        <Button
          key={`button-${index}`}
          title={`Name: ${result.name} Username: ${result.username}`}
          onPress={() => navigation.navigate('Profile', {user: result})}
        />,
      ];
    });
  }

  render() {
    // @ts-ignore
    const {navigation} = this.props;
    return (
      <ScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <TextInput
          style={{
            height: 40,
            width: '75%',
            borderColor: 'gray',
            borderWidth: 1,
          }}
          onChangeText={(text) => this.onChangeText(text)}
        />
        <Text>Search Results:</Text>
        {this.renderUsers(navigation)}
      </ScrollView>
    );
  }
}

const styles = styled.ScrollView.attrs(() => ({
  contentContainerStyle: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

export default function (props: any) {
  const navigation = useNavigation();

  return <Home {...props} navigation={navigation} />;
}
