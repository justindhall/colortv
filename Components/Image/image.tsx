import * as React from 'react';
import {View, ImageBackground, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

class Image extends React.Component {
  render() {
    // @ts-ignore
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ImageBackground
          style={styles.full}
          source={{uri: this.props.route.params.image.urls.full}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  full: {
    width: '100%',
    height: '100%',
  },
});

export default function (props: any) {
  const navigation = useNavigation();

  return <Image {...props} navigation={navigation} />;
}
