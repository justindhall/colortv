import * as React from 'react';
import {
  Dimensions,
  Text,
  View,
  Image,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

class Profile extends React.Component {
  calculateSize() {
    const size = windowWidth / 3;
    return {width: size, height: size};
  }

  renderImages(navigation: any) {
    return this.props.route.params.user.photos.map(
      (photo: any, index: number) => {
        return [
          <TouchableHighlight
            key={`hightlight-${index}`}
            onPress={() => navigation.navigate('Image', {image: photo})}>
            <Image
              key={`image-${index}`}
              style={styles.image}
              source={{uri: photo.urls.thumb}}
            />
          </TouchableHighlight>,
        ];
      },
    );
  }

  render() {
    // @ts-ignore
    const {navigation} = this.props;
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text>{this.props.route.params.user.name}</Text>
        <Image
          style={styles.profile}
          source={{uri: this.props.route.params.user.profile_image.large}}
        />
        {this.renderImages(navigation)}
      </View>
    );
  }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  image: {
    width: windowWidth / 3,
    height: windowWidth / 3,
  },
  profile: {
    width: 128,
    height: 128,
  },
});

export default function (props: any) {
  const navigation = useNavigation();

  return <Profile {...props} navigation={navigation} />;
}
