import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Profile from './Components/Profile/profile';
import Home from './Components/Home/home';
import Image from './Components/Image/image';
import {createStore} from 'redux';

function photos(state = [], action: any) {
  return [...state, action.type];
}

let store = createStore(photos);

store.subscribe(() => console.log(store.getState()));

const RootStack = createStackNavigator();
const MainStack = createStackNavigator();

function MainStackScreen() {
  return (
    <MainStack.Navigator>
      <MainStack.Screen name="Home" component={Home} />
      <MainStack.Screen name="Profile" component={Profile} />
    </MainStack.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <RootStack.Navigator mode="modal">
        <RootStack.Screen
          name="Back"
          component={MainStackScreen}
          options={{headerShown: false}}
        />
        <RootStack.Screen name="Image" component={Image} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

export default App;
